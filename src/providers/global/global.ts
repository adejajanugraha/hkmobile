import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {
  public nav_color: string;
  base_url: any;
  constructor(private storage: Storage) {
    this.storage.get('base_url').then((val) => {
      this.base_url = val;
      if (val=='http://192.168.68.92/hkfe/') this.nav_color= 'prod';
      else this.nav_color = 'qa';
    });
  }

  refreshColor() {
    this.storage.get('base_url').then((val) => {
      this.base_url = val;
      if (val=='http://192.168.68.92/hkfe/') this.nav_color= 'prod';
      else this.nav_color = 'qa';
    });
  }
}
