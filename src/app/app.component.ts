import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { Storage } from '@ionic/storage';

import {Http, Headers } from '@angular/http';
import { Events } from 'ionic-angular';
import { GlobalProvider } from '../providers/global/global';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon : any, badge : any}>;
  loggedIn: any;
  username: any;
  password: any;
  loading: any;
  base_url: string;
  nav_color: any;
  navcol: any;
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private alertCtrl: AlertController, private storage: Storage, public http:Http, public events: Events, public loadingCtrl : LoadingController, public global: GlobalProvider) {
    this.initializeApp();
    this.storage.get('base_url').then((val) => {
      this.base_url = val;
    });
    this.pages = [
        { title: 'Home', component: HomePage, icon: 'home', badge : 'X' },
    ];
    
    events.subscribe('navc', navcol => {
      this.navcol = navcol;
    });
  }

  presentLoadingDefault(content) {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'+content,
      dismissOnPageChange:false
    });

    this.loading.present();
  }

  logout() {
    let alert = this.alertCtrl.create({
        title: 'Confirm Logout',
        message: 'Do you want to logout?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              localStorage.clear();
              this.nav.setRoot(LoginPage);
            }
          }
        ]
      });
      alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}
