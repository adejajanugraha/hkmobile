import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { LoadingController,AlertController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import {Http, Headers } from '@angular/http';
import { Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

import { NgZone } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { CompleterService, CompleterData, CompleterItem, RemoteData } from 'ng2-completer';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { CurrencyPipe } from '@angular/common';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { Chart } from 'chart.js';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('barGrafikLaba') barGrafikLaba;
  @ViewChild('doughnutCanvas') doughnutCanvas;

  grafikLaba: any;
  barChart: any;
  doughnutChart: any;

  loading: any;
  rootPage: typeof LoginPage;
  loggedIn: any;
  username: any;
  password: any;
 // group_user : any;

  nav_color: any;
  base_url: string;


  constructor(private storage: Storage, public http:Http, public navCtrl: NavController,public loadingCtrl: LoadingController, private alertCtrl: AlertController, public events: Events, public global: GlobalProvider) {
    this.storage.get('base_url').then((val) => {
        this.base_url=val;
        if (val=='http://192.168.68.92/hkfe/') this.nav_color= 'prod';
        else this.nav_color = 'qa';
        this.events.publish('navc', this.nav_color);
    });
    this.global.refreshColor();

    this.init();
  }

  init(){
    this.storage.get('isLoggedIn').then((isLoggedIn) => {
        console.log('Is Logged in : ', isLoggedIn);
        this.loggedIn = isLoggedIn;
        if(!this.loggedIn){
            this.navCtrl.setRoot(LoginPage);
        }
        else if(this.loggedIn) {
            this.storage.get('username').then((val) => {
              this.username=val;
              this.storage.get('password').then((val) => {
                this.password=val;

              });
            });
        }
    });
  }

 
  ionViewDidEnter (){
    this.storage.get('base_url').then((val) => {
        if (val!=null){
          this.storage.get('username').then((val) => {

          });
        }
    });
  }  


  ionViewDidLoad() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
          labels: ["Aset Lancat", "Aset Tidak Lancar"],
          datasets: [{
              label: '# of Votes',
              data: [12, 19],
              backgroundColor: [
                "#FF6384",
                "#36A2EB"
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB"
              ]
          }]
      }

  });



    this.grafikLaba = new Chart(this.barGrafikLaba.nativeElement, {

      type: 'bar',
      data: {
          labels: ["Kantor Pusat", "Proyek HK Infra 89", "UAT Report  PS", "Proyek 2019"],
          datasets: [{
              label: 'Laba',
              data: [12, 19, 3, 5],
              backgroundColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 1)'
              ]
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }

  });


    this.barChart = new Chart(this.barCanvas.nativeElement, {

        type: 'bar',
        data: {
            labels: ["201803", "201804", "201805", "201806", "201807", "201808"],
            datasets: [{
                label: 'PO',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                  'rgba(255,99,132,1)',
                  'rgba(255,99,132,1)',
                  'rgba(255,99,132,1)',
                  'rgba(255,99,132,1)',
                  'rgba(255,99,132,1)',
                  'rgba(255,99,132,1)'
                ]
            },
            {
              label: 'PR',
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)'
              ]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }

    });
  }



  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Session timed out',
      subTitle: 'Please Login to Continue',
      buttons: ['OK']
    });
    alert.present();
  }

  presentLoadingDefault(content='') {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'+content,
      dismissOnPageChange:true
    });

    this.loading.present();
  }



}
