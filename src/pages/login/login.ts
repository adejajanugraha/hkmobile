import { Component } from '@angular/core';
import { NavController, Keyboard } from 'ionic-angular';
import { LoadingController, MenuController } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  username: any;
  temp_username: any;
  password: any;
  loading: any;
  base_url='http://192.168.68.92/hkfe/';
  constructor(private storage: Storage, public http:Http, public navCtrl: NavController, public loadingCtrl: LoadingController, private alertCtrl: AlertController,private menu: MenuController, public keyboard: Keyboard, public events: Events, public global: GlobalProvider) {
    this.storage.clear();
    this.storage.set('username', null);
    this.storage.set('group_user', null);
    this.storage.set('isLoggedIn', false);
    this.menu.swipeEnable(false);
  }

  presentAlert(xTitle, xSubTitle) {
    let alert = this.alertCtrl.create({
      title: xTitle,
      subTitle: xSubTitle,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange:true,
      enableBackdropDismiss:true
    });

    loading.present();
  }

  login () {
    this.storage.clear();
    this.storage.set('listAP', null);
    this.presentLoadingDefault();
    let headers = new Headers();
    this.temp_username = this.username;
    if (this.username.substring(0, 4).toUpperCase() == 'DEV_') {
      this.base_url = 'http://192.168.68.92/hkfe/';
      this.username = this.username.substring(4);
    }else if (this.username.substring(0, 6).toUpperCase() == 'LOCAL_') {
      this.base_url = 'http://localhost/hkfe/';
      this.username = this.username.substring(6);
    }
 
    console.log(this.base_url);
    headers.append('Content-Type', 'application/json');
    headers.append('X-API-KEY', 'befea63a-7c47-4323-afea-968a3168fe0a');
    this.http.get(this.base_url+'api/login?username='+this.username+'&password='+this.password, {headers: headers})
    .map(res => res.json())
    .subscribe(res => {
      if(res!='false'){
        // console.log('abcdefg '+res);
        this.storage.set('username', this.username);
        this.storage.set('password', this.password);
        // //get counter
        // this.getListApproveAP(this.username);
        // this.getListPR(this.username);
        this.storage.set('isLoggedIn', true);
        this.storage.set('base_url', this.base_url);
        this.navCtrl.insert(0,HomePage);
        this.navCtrl.popToRoot();
        
      }
      else {
        this.presentAlert('Login Failed', 'Invalid Username or Password');
        this.username = this.temp_username;
        this.base_url='http://192.168.68.92/hkfe/';
        this.loading.dismiss();      
      }
      }, (err) => {
        // if (String(err).substr(err.length - 4) == 'null') {
        // err = 'Connection failed, please check your internet connection or cantact administrator if the problem persist'
      // }
      this.presentAlert('Login Failed', err);
      this.username = this.temp_username;
      this.loading.dismiss();      

    });

  }
  
}
